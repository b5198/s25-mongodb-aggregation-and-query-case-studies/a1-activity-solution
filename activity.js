db.products.aggregate([
	{$match: {supplier:"Red Farms Inc."}},
	{$count:"RedFarmsTotalItems"}
])

db.products.aggregate([
	{$match: {price:{$gt:50}}},
	{$count:"Items w Price Greater than 50"}
])

db.products.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:"$supplier",avgPrice:{$avg:"$price"}}}
])

db.products.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:"$supplier",maxPrice:{$max:"$price"}}}
])

db.products.aggregate([
	{$match: {onSale:true}},
	{$group:{_id:"$supplier",minPrice:{$min:"$price"}}}
])